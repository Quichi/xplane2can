# XPlane2CAN - Plugin for Xplane11

This plugin reads datarefs and sends then via a CAN-Interface.<br>
The desired messages are specified in the x2c.txt and c2x.txt files in the XPlane root directory.
 ## File format
 ### Datasets
 The x2c and c2x are comma seperated sets representing the messages.<br>
 The first entry is the dataref, the second is the CAN-ID and the third entry is the CAN-Aerospace datatype.<br>
 For example:<br><br>
 `sim/flightmodel/misc/h_ind, 1310, FLOAT`<br>
 The **indicated barometric altitude** is sent as **FLOAT**-Value with the CAN-ID **1310**.
 ### Configuration
 The xp2c_config.ini provides basic configuration for the plugin.
 - **CAN** - CAN-Configuration<br>
 `device`: Not supported for now<br>
 `interface`:  CAN-Interface to use. (usb0 or usb1 for PEAKCan-Adapter)<br>
 `baudrate`: Baudrate (CAN_1M, CAN_500K, CAN_250K, CAN_125K)
- **NODE** - CAN-Node and Heartbeat Configuration<br>
`nodeId`: Node Id of the "Sim"<br>
`phsmId`: Heartbeat Id of the "Sim"<br>
`pshmPeriod`: Heartbeat Period
- **X2C** - XPlane output Configuration<br>
`frequency`: Frequency at which the CAN-Messages are generated.


 
