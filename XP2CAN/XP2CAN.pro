TEMPLATE = lib
QT -= gui
QT += core
QT += serialbus
QT += widgets

CONFIG += warn_on plugin c++17 qt
CONFIG -= thread  rtti debug_and_release_target

VERSION = 1.0.0

DEFINES += XPLM200
DEFINES += XPLM210
DEFINES += XPLM300
DEFINES += XPLM301
DEFINES += XPLM302
DEFINES += XPLM303

debug {
    DEFINES += DEBUG
} else {
    CONFIG += release
}

win32 {
    INCLUDEPATH += "D:/Workspace/XP2CAN/SDK/CHeaders/XPLM"
    INCLUDEPATH += "D:/Workspace/XP2CAN/SDK/CHeaders/Wrappers"
    INCLUDEPATH += "D:/Workspace/XP2CAN/SDK/CHeaders/Widgets"
    LIBS += -L"D:/Workspace/XP2CAN/SDK/Libraries/Win"
    LIBS += -lXPLM_64 -lXPWidgets_64
    LIBS += -L"D:/Workspace/XP2CAN/lib/x64"
    LIBS += -lPCANBasic
    DEFINES += APL=0 IBM=1 LIN=0
    TARGET_EXT = .xpl
}

unix:!macx {
    INCLUDEPATH += "D:/Workspace/XP2CAN/SDK/CHeaders/XPLM"
    INCLUDEPATH += "D:/Workspace/XP2CAN/SDK/CHeaders/Wrappers"
    INCLUDEPATH += "D:/Workspace/XP2CAN/SDK/CHeaders/Widgets"
    CONFIG += no_plugin_name_prefix
    DEFINES += APL=0 IBM=0 LIN=1
    QMAKE_EXTENSION_SHLIB = xpl
    QMAKE_CXXFLAGS += -fvisibility=hidden -shared -rdynamic -nodefaultlibs -undefined_warning
}

macx {
    TARGET = mac
    TARGET_EXT = xpl
    DEFINES += APL=1 IBM=0 LIN=0
    QMAKE_LFLAGS += -undefined suppress
    CONFIG += x64 ppc
}

SOURCES +=     plugin.cpp     dllmainwindows.cpp \
    can.cpp \
    can_message.cpp \
    csv.cpp \
    x2c.cpp \
    xpDataAccess.cpp
	
HEADERS +=     dllmainwindows.h \
    can.h \
    csv.h \
    x2c.h \
    xpDataAccess.h \
    PCANBasic.h \

DISTFILES += \
    x2c.txt \
    xp2c_config.ini
