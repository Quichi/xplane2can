#include "can.h"
#include "PCANBasic.h"
#include "XPLMUtilities.h"

CAN::CAN()
{
}

int CAN::connect(QString baudrateString, QString interfaceString)
{
  TPCANStatus stsResult;

  // Set Baudrate
  if(baudrateString == "CAN_1M")
    _baudrate = PCAN_BAUD_1M;
  else if (baudrateString == "CAN_500K")
    _baudrate = PCAN_BAUD_500K;
  else if (baudrateString == "CAN_250K")
    _baudrate = PCAN_BAUD_250K;
  else if (baudrateString == "CAN_125K")
    _baudrate = PCAN_BAUD_125K;
  else if (baudrateString == "CAN_83K")
    _baudrate = PCAN_BAUD_83K;
  else
  {
      XPLMDebugString("Invalid Baudrate (");
      XPLMDebugString(baudrateString.toLocal8Bit().constData());
      XPLMDebugString(") - set to default (1M)\n");
      _baudrate = PCAN_BAUD_1M;
  }

  // Set interface
  if(interfaceString == "usb0")
    _pcanHandle = PCAN_USBBUS1;
  else if (interfaceString == "usb1")
    _pcanHandle = PCAN_USBBUS2;
  else
  {
      XPLMDebugString("Invalid Interface (");
      XPLMDebugString(interfaceString.toLocal8Bit().constData());
      XPLMDebugString(") - set to default (usb0)\n");
      _pcanHandle = PCAN_USBBUS1;
  }

  stsResult = CAN_Initialize(_pcanHandle, _baudrate);
  if (stsResult != PCAN_ERROR_OK)
    {
      XPLMDebugString("Can not initialize CAN-Bus on ");
      XPLMDebugString(interfaceString.toLocal8Bit().simplified().constData());
      XPLMDebugString("\nPlease check the configuration.\n");
      return stsResult;
    }
  XPLMDebugString("CAN-Bus init successful.\n");
  return stsResult;
}

CAN::~CAN()
{
  CAN_Uninitialize(PCAN_NONEBUS);
}

int CAN::send(TPCANMsg canFrame)
{
  TPCANStatus status = CAN_Write(_pcanHandle, &canFrame);
  return status;
}

