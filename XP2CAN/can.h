#ifndef CAN_H
#define CAN_H

#include <QString>
#include "PCANBasic.h"

class CAN
{
private:
  TPCANHandle _pcanHandle = PCAN_USBBUS1;
  TPCANBaudrate _baudrate = PCAN_BAUD_1M;
    QString _device;
    QString _interface;

public:
    CAN();
    ~CAN();
    int connect(QString baudrateString, QString interfaceString);
    int send(TPCANMsg canFrame);
};

#endif // CAN_H
