/*
Author: asdf
Version: 1.0
Version date: 15.08.2022
*/
#if IBM
#include <windows.h>
#endif
#include <string>

#include <QFile>
#include <QSettings>

#include "XPLMPlugin.h"
#include "XPLMUtilities.h"
#include "XPLMDataAccess.h"
#include "XPLMProcessing.h"

#include "csv.h"
#include "x2c.h"
#include "can.h"

#define MAX_NUMBER_X2C  32

const QString FILENAME_INI = "xp2c_config.ini";
const QString FILENAME_X2C = "x2c.txt";
const QString FILENAME_C2X = "c2x.txt";

CAN canDevice;
X2C xplane2can[MAX_NUMBER_X2C];
int x2cDatasets = 0;

QSettings *settings;

CAN_Message heartbeatMessage;

//#include "dllmainwindows.h"

int readX2C(const QString filename);
int readConfig(const QString filename);
float processLoop(float elapsedMe, float elapsedSim, int counter, void * refcon);
float heartbeat(float elapsedMe, float elapsedSim, int counter, void * refcon);

PLUGIN_API int XPluginStart(char * outName, char * outSig, char * outDesc)
{
    strcpy(outName, "XP2CAN");
    strcpy(outSig, "XPlane to CAN plugin");
    strcpy(outDesc, " ");


    XPLMDebugString("XPlane2CAN Plugin found\n");

    // read the configuration
    readConfig(FILENAME_INI);
    // read the files first
    readX2C(FILENAME_X2C);

    XPLMRegisterFlightLoopCallback(&processLoop, 1, NULL);
    XPLMRegisterFlightLoopCallback(&heartbeat, 1, NULL);

    return 1;
}

PLUGIN_API void XPluginReceiveMessage(XPLMPluginID inFrom, int inMessage, void * inParam)
{
    // unused arguments
    (void) inFrom;
    (void) inMessage;
    (void) inParam;
}

PLUGIN_API void  XPluginStop(void)
{
    XPLMUnregisterFlightLoopCallback(&processLoop, NULL);
    XPLMUnregisterFlightLoopCallback(&heartbeat, NULL);
}

PLUGIN_API void XPluginDisable(void) {}

PLUGIN_API int XPluginEnable(void) { return 1; }

int readX2C(const QString filename)
{
    QString debugString {"Loading x2c Datasets\n"};
    XPLMDebugString(debugString.toLocal8Bit());

    x2cDatasets = 0;
    QFile x2cFile(filename);
    if(x2cFile.open(QFile::ReadOnly | QFile::Text))
    {
        QTextStream in(&x2cFile);
        QStringList row;
        while (csv_readRow(in, &row))
        {
            xplane2can[x2cDatasets] = X2C(row[0], row[1], row[2]);
            x2cDatasets++;
        }
        debugString = QString::number(x2cDatasets).append(" Lines read\n");
        XPLMDebugString(debugString.toLocal8Bit().constData());
    }
    else
    {
        XPLMDebugString("X2C - File ");
        XPLMDebugString(filename.toLocal8Bit().constData());
        XPLMDebugString(" not found\n");
    }
    return x2cDatasets;
}

int readConfig(const QString filename)
{
    XPLMDebugString("Reading Configuration\n");
    settings = new QSettings(filename, QSettings::IniFormat);

    heartbeatMessage.setCanId(settings->value("NODE/phsmId", "1000").toUInt());
    heartbeatMessage.setNodeId(settings->value("NODE/nodeId", "42").toUInt());
    heartbeatMessage.setType(AS_ULONG);

    canDevice.connect(settings->value("CAN/baudrate", "CAN_1M").toString(), settings->value("CAN/interface", "usb0").toString());

    return 1;
}

float processLoop(float elapsedMe, float elapsedSim, int counter, void * refcon)
{
    TPCANMsg canFrameRaw;
    int x2cHandlers = 0;
    for(x2cHandlers = 0; x2cHandlers < x2cDatasets; x2cHandlers++)
    {
        if(xplane2can[x2cHandlers].process())
        {
            canFrameRaw = xplane2can[x2cHandlers].getCanMessage();
            canDevice.send(canFrameRaw);
        }
    }
    float freq = fabs(settings->value("X2C/frequency","1.0").toFloat());
    return 1.0/freq;
    //return interval;
}

float heartbeat(float elapsedMe, float elapsedSim, int counter, void * refcon)
{
    static int cnt = 0;

    heartbeatMessage.setPayloadLong(++cnt);
    canDevice.send(heartbeatMessage.getCanFrame());
    return settings->value("NODE/phsmPeriod","1").toFloat();
}
