#ifndef X2C_H
#define X2C_H

#include <stdint.h>

#include <QString>

#include "XPLMDataAccess.h"

#include "can_message.h"

class X2C
{
private:
    XPLMDataRef _dataref;
    uint8_t     _datatype;

    CAN_Message _canMessage;
public:
    X2C();
    X2C(QString datarefStr, QString canIdStr, QString typeStr);
    bool isValid();
    bool process();
    TPCANMsg getCanMessage();
};

#endif // X2C_H
