#include "can_message.h"
#include "QByteArray"

CAN_Message::CAN_Message()
{

}

CAN_Message::CAN_Message(uint16_t canId)
{
    _canId = canId;
    _type = AS_NODATA;
    _nodeId = 0;
    _svcCode = 0;
    _msgCode = 0;
}

CAN_Message::CAN_Message(uint16_t canId, uint8_t type)
{
    _canId = canId;
    _type = type;
    _nodeId = 0;
    _svcCode = 0;
    _msgCode = 0;
}

CAN_Message::CAN_Message(uint16_t canId, uint8_t nodeId, uint8_t type, uint8_t svcCode, uint8_t msgCode)
{
    _canId = canId;
    _type = type;
    _nodeId = nodeId;
    _svcCode = svcCode;
    _msgCode = msgCode;
}

void CAN_Message::setNodeId(uint8_t nodeId)
{
    _nodeId = nodeId;
}
void CAN_Message::setCanId(uint16_t canId)
{
    _canId = canId;
}
void CAN_Message::setMsgCode(uint8_t msgCode)
{
    _msgCode = msgCode;
}
void CAN_Message::setSvcCode(uint8_t svcCode)
{
    _svcCode = svcCode;
}
void CAN_Message::setType(uint8_t type)
{
    _type = type;
}
void CAN_Message::setType(char* typeString)
{

}

void CAN_Message::setPayloadFloat(float value)
{
    uint8_t *val = (uint8_t *) &value;
    _payload[0] = val[3] & 0xff;
    _payload[1] = val[2] & 0xff;
    _payload[2] = val[1] & 0xff;
    _payload[3] = val[0] & 0xff;
}
void CAN_Message::setPayloadLong(unsigned long value)
{
    uint8_t *val = (uint8_t *) &value;
    _payload[0] = val[3] & 0xff;
    _payload[1] = val[2] & 0xff;
    _payload[2] = val[1] & 0xff;
    _payload[3] = val[0] & 0xff;
}
void CAN_Message::setPayloadShort(unsigned short value)
{
    uint8_t *val = (uint8_t *) &value;
    _payload[0] = val[1] & 0xff;
    _payload[1] = val[0] & 0xff;
    _payload[2] = 0;
    _payload[3] = 0;
}
void CAN_Message::setPayloadShort2(unsigned short value1, unsigned short value2)
{
    uint8_t *val1 = (uint8_t *) &value1;
    uint8_t *val2 = (uint8_t *) &value2;
    _payload[0] = val1[1] & 0xff;
    _payload[1] = val1[0] & 0xff;
    _payload[2] = val2[1] & 0xff;
    _payload[3] = val2[0] & 0xff;
}
void CAN_Message::setPayloadChar1(unsigned char c1)
{
    _payload[0] = c1;
    _payload[1] = 0;
    _payload[2] = 0;
    _payload[3] = 0;
}
void CAN_Message::setPayloadChar2(unsigned char c1, unsigned char c2)
{
    _payload[0] = c1;
    _payload[1] = c2;
    _payload[2] = 0;
    _payload[3] = 0;
}
void CAN_Message::setPayloadChar3(unsigned char c1, unsigned char c2, unsigned char c3)
{
    _payload[0] = c1;
    _payload[1] = c2;
    _payload[2] = c3;
    _payload[3] = 0;
}
void CAN_Message::setPayloadChar4(unsigned char c1, unsigned char c2, unsigned char c3, unsigned char c4)
{

    _payload[0] = c1;
    _payload[1] = c2;
    _payload[2] = c3;
    _payload[3] = c4;
}

TPCANMsg CAN_Message::getCanFrame()
{
    TPCANMsg canFrame;

    canFrame.ID = _canId;
    canFrame.LEN = 8;
    canFrame.MSGTYPE = PCAN_MESSAGE_STANDARD;

    canFrame.DATA[0] = _nodeId;
    canFrame.DATA[1] = _type;
    canFrame.DATA[2] = _svcCode;
    canFrame.DATA[3] = _msgCode;
    canFrame.DATA[4] = _payload[0];
    canFrame.DATA[5] = _payload[1];
    canFrame.DATA[6] = _payload[2];
    canFrame.DATA[7] = _payload[3];

    return canFrame;
}
