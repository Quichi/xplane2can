#ifndef CAN_MESSAGE_H
#define CAN_MESSAGE_H

#include <stdint.h>
#include "PCANBasic.h"

enum
{
    AS_NODATA,
    AS_ERROR,
    AS_FLOAT,
    AS_LONG,
    AS_ULONG,
    AS_BLONG,
    AS_SHORT,
    AS_USHORT,
    AS_BSHORT,
    AS_CHAR,
    AS_UCHAR,
    AS_BCHAR,
    AS_SHORT2,
    AS_USHORT2,
    AS_BSHORT2,
    AS_CHAR4,
    AS_UCHAR4,
    AS_BCHAR4,
    AS_CHAR2,
    AS_UCHAR2,
    AS_BCHAR2,
    AS_MEMID,
    AS_CHKSUM,
    AS_ACHAR,
    AS_ACHAR2,
    AS_ACHAR4,
    AS_CHAR3,
    AS_UCHAR3,
    AS_BCHAR3,
    AS_ACHAR3,
};

class CAN_Message
{
private:
    uint16_t    _canId;
    uint8_t     _nodeId;
    uint8_t     _type;
    uint8_t     _svcCode;
    uint8_t     _msgCode;
    uint8_t     _payload[4];

public:
    CAN_Message();
    CAN_Message(uint16_t canId);
    CAN_Message(uint16_t canId, uint8_t type);
    CAN_Message(uint16_t canId, uint8_t nodeId, uint8_t type, uint8_t svcCode, uint8_t msgCode);
    CAN_Message(uint8_t canRaw[8]);

    void setNodeId(uint8_t nodeId);
    void setCanId(uint16_t canId);
    void setMsgCode(uint8_t msgCode);
    void setSvcCode(uint8_t svcCode);
    void setType(uint8_t type);
    void setType(char* typeString);

    uint8_t getNodeId();
    uint16_t getCanId();
    uint8_t getMsgCode();
    uint8_t getSvcCode();
    uint8_t getType();

    void setPayloadFloat(float value);
    void setPayloadLong(unsigned long value);
    void setPayloadShort(unsigned short value);
    void setPayloadShort2(unsigned short value1, unsigned short value2);
    void setPayloadChar1(unsigned char c1);
    void setPayloadChar2(unsigned char c1, unsigned char c2);
    void setPayloadChar3(unsigned char c1, unsigned char c2, unsigned char c3);
    void setPayloadChar4(unsigned char c1, unsigned char c2, unsigned char c3, unsigned char c4);

    bool getPayloadFloat(float *value);
    bool getPayloadLong(unsigned long *value);
    bool getPayloadShort(unsigned short *value);
    bool getPayloadShort2(unsigned short *value1, unsigned short *value2);
    bool getPayloadChar1(unsigned char *c1);
    bool getPayloadChar2(unsigned char *c1, unsigned char *c2);
    bool getPayloadChar3(unsigned char *c1, unsigned char *c2, unsigned char *c3);
    bool getPayloadChar4(unsigned char *c1, unsigned char *c2, unsigned char *c3, unsigned char *c4);

    TPCANMsg getCanFrame();
};

#endif // CAN_MESSAGE_H
