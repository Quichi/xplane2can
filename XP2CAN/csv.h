#ifndef CSV_H
#define CSV_H

#include <QTextStream>
#include <QStringList>

bool csv_readRow(QTextStream &in, QStringList *row);

#endif // CSV_H
