#include "x2c.h"

#include <QString>
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "can_message.h"

X2C::X2C()
{

}

X2C::X2C(QString datarefStr, QString canIdStr, QString typeStr)
{
    XPLMDebugString("Dataref: ");
    _dataref = XPLMFindDataRef(datarefStr.toLocal8Bit().simplified().constData());
    if(_dataref != nullptr)
    {
        XPLMDebugString(datarefStr.simplified().toLocal8Bit().constData());
        // get CAN-ID
        bool canIdOk = false;
        uint16_t canId = canIdStr.simplified().toUShort(&canIdOk);
        if(canIdOk == false)
        {
            XPLMDebugString(canIdStr.simplified().toLocal8Bit());
            XPLMDebugString(" invalid CAN-ID\n");
            return;
        }

        typeStr = typeStr.simplified();
        // interpret type
        if(typeStr == "NODATA")
            _datatype = AS_NODATA;
        else if (typeStr == "ERROR")
            _datatype = AS_ERROR;
        else if (typeStr == "FLOAT")
            _datatype = AS_FLOAT;
        else if (typeStr == "LONG")
            _datatype = AS_LONG;
        else if (typeStr == "ULONG")
            _datatype = AS_ULONG;
        else if (typeStr == "BLONG")
            _datatype = AS_BLONG;
        else if (typeStr == "SHORT")
            _datatype = AS_SHORT;
        else if (typeStr == "USHORT")
            _datatype = AS_USHORT;
        else if (typeStr == "BSHORT")
            _datatype = AS_BSHORT;
        else if (typeStr == "CHAR")
            _datatype = AS_CHAR;
        else if (typeStr == "UCHAR")
            _datatype = AS_UCHAR;
        else if (typeStr == "BCHAR")
            _datatype = AS_BCHAR;
        else if (typeStr == "SHORT2")
            _datatype = AS_SHORT2;
        else if (typeStr == "USHORT2")
            _datatype = AS_USHORT2;
        else if (typeStr == "BSHORT2")
            _datatype = AS_BSHORT2;
        else if (typeStr == "CHAR4")
            _datatype = AS_CHAR4;
        else if (typeStr == "UCHAR4")
            _datatype = AS_UCHAR4;
        else if (typeStr == "BCHAR4")
            _datatype = AS_BCHAR4;
        else if (typeStr == "CHAR2")
            _datatype = AS_CHAR2;
        else if (typeStr == "UCHAR2")
            _datatype = AS_UCHAR2;
        else if (typeStr == "BCHAR2")
            _datatype = AS_BCHAR2;
        else if (typeStr == "MEMID")
            _datatype = AS_MEMID;
        else if (typeStr == "CHKSUM")
            _datatype = AS_CHKSUM;
        else if (typeStr == "ACHAR")
            _datatype = AS_ACHAR;
        else if (typeStr == "ACHAR2")
            _datatype = AS_ACHAR2;
        else if (typeStr == "ACHAR4")
            _datatype = AS_ACHAR4;
        else if (typeStr == "CHAR3")
            _datatype = AS_CHAR3;
        else if (typeStr == "UCHAR3")
            _datatype = AS_UCHAR3;
        else if (typeStr == "BCHAR3")
            _datatype = AS_BCHAR3;
        else if (typeStr == "ACHAR3")
            _datatype = AS_ACHAR3;
        else
        {
            _datatype = AS_NODATA;
            XPLMDebugString("invalid DataType: ");
            XPLMDebugString(typeStr.simplified().toLocal8Bit().constData());
            XPLMDebugString("\n");
            return;
        }
        _canMessage = CAN_Message((uint16_t) canId, (uint8_t) _datatype);
        XPLMDebugString(" on CAN-ID ");
        XPLMDebugString(canIdStr.simplified().toLocal8Bit().constData());
        XPLMDebugString(" is valid\n");
    }
    else
    {
        XPLMDebugString(datarefStr.simplified().toLocal8Bit().constData());
        XPLMDebugString(" invalid\n");
    }
}

bool X2C::isValid()
{
    if(_dataref != nullptr)
        return true;
    else
        return false;
}

bool X2C::process()
{
    if(isValid())
    {
        float dataF = 0.0;
        int dataL = 0;
        int dataS[2] = {0};
        char dataC[4] = {0};

        // get dataref type
        XPLMDataTypeID datarefType = XPLMGetDataRefTypes(_dataref);
        // read dataref according to type
        switch(datarefType)
        {
        case xplmType_Float:
        case xplmType_Double:
            dataF = XPLMGetDataf(_dataref);
            break;
        case xplmType_Int:
            dataL = (uint32_t) XPLMGetDatai(_dataref);
            dataS[0] = (uint16_t) XPLMGetDatai(_dataref);
            break;
        case xplmType_IntArray:
           XPLMGetDatavi(_dataref, dataS, 0, 2);
            break;
        case xplmType_Data:
            XPLMGetDatab(_dataref, dataC, 0, 4);
            break;
        default:
            break;
        }


        if(_datatype == AS_NODATA)
        {

        }
        else if(_datatype == AS_ERROR)
        {

        }
        else if(_datatype == AS_FLOAT)
        {
            _canMessage.setPayloadFloat(dataF);
        }
        else if((_datatype == AS_LONG) || (_datatype == AS_ULONG) || (_datatype == AS_BLONG))
        {
            _canMessage.setPayloadLong(dataL);
        }
        else if((_datatype == AS_SHORT) || (_datatype == AS_USHORT) || (_datatype == AS_BSHORT))
        {
            _canMessage.setPayloadShort((uint16_t) dataS[0]);
        }
        else if((_datatype == AS_SHORT2) || (_datatype == AS_USHORT2) || (_datatype == AS_BSHORT2))
        {
            _canMessage.setPayloadShort2((uint16_t) dataS[0], (uint16_t) dataS[1]);
        }
        else if((_datatype == AS_CHAR) || (_datatype == AS_UCHAR) || (_datatype == AS_BCHAR) || (_datatype == AS_ACHAR))
        {
            _canMessage.setPayloadChar1((uint8_t) dataC[0]);
        }
        else if((_datatype == AS_CHAR2) || (_datatype == AS_UCHAR2) || (_datatype == AS_BCHAR2) || (_datatype == AS_ACHAR2))
        {
            _canMessage.setPayloadChar2((uint8_t) dataC[0], (uint8_t) dataC[1]);
        }
        else if((_datatype == AS_CHAR3) || (_datatype == AS_UCHAR3) || (_datatype == AS_BCHAR3) || (_datatype == AS_ACHAR3))
        {
            _canMessage.setPayloadChar3((uint8_t) dataC[0], (uint8_t) dataC[1], (uint8_t) dataC[2]);
        }
        else if((_datatype == AS_CHAR4) || (_datatype == AS_UCHAR4) || (_datatype == AS_BCHAR4) || (_datatype == AS_ACHAR4))
        {
            _canMessage.setPayloadChar4((uint8_t) dataC[0], (uint8_t) dataC[1], (uint8_t) dataC[2], (uint8_t) dataC[3]);
        }
    }
    else
        return false;

    return true;
}

TPCANMsg X2C::getCanMessage()
{
    return _canMessage.getCanFrame();
}
