#include "xpDataAccess.h"

#include <string.h>

#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"

int readDataRef(char* dataref_string)
{
    int ret = 0;
    XPLMDataRef dataref = XPLMFindDataRef(dataref_string);
    if(dataref != nullptr)
    {
        ret = 1;
        XPLMDebugString("Dataref: ");
        XPLMDebugString(dataref_string);
        XPLMDebugString(" Found\n");
    }
    else
    {
        XPLMDebugString("Invalid Dataref\n");
    }

    return ret;
}
