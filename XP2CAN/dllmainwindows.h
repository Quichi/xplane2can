#ifndef DLLMAINWINDOWS_H
#define DLLMAINWINDOWS_H

#if IBM
#include <windows.h>
BOOL APIENTRY DllMain( HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved);
#endif

#endif // DLLMAINWINDOWS_H
